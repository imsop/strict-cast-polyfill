<?php declare(strict_types=1);

function type_cast(string $type, $input)
{
    switch ( $type ) {
        case 'int':
            if ( is_int($input) ) {
                return $input;
            }
            elseif (
                is_string($input)
                && preg_match('/^-?[0-9]+$/', $input)
                && ! ( (float)$input > PHP_INT_MAX || (float)$input < PHP_INT_MIN )
            ) {
                return (int)$input;
            }
            elseif (
                is_float($input)
                && !is_nan($input)
                && !is_infinite($input)
                && ! ( $input > PHP_INT_MAX || $input < PHP_INT_MIN )
                && floor($input) === $input
            ) {
                return (int)$input;
            }
            elseif ( $input === true ) {
                return 1;
            }
            elseif ( $input === false ) {
                return 0;
            }
        break;
        case 'float':
            if ( is_float($input) ) {
                return $input;
            }
            elseif ( is_int($input) ) {
                return (float)$input;
            }
            elseif ( $input === true ) {
                return 1.0;
            }
            elseif ( $input === false ) {
                return 0.0;
            }
            elseif ( $input === 'NAN' ) {
                return NAN;
            }
            elseif ( $input === 'INF' ) {
                return INF;
            }
            elseif ( $input === '-INF' ) {
                return -INF;
            }
            elseif (
                is_string($input)
                && preg_match('/^-?[0-9]+(\.[0-9]+)?$/', $input)
                && ! is_infinite( (float)$input )
            ) {
                return (float)$input;
            }
        break;
    }

    throw new TypeError("Value cannot be cast to type $type.");
}
