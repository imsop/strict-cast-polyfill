<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class IntTest extends TestCase
{
    public function data_provider_valid_integer_inputs(): iterable
    {
        yield 'Integer' => [42, 42];
        yield 'Negative Integer' => [-42, -42];

        yield 'All-digit string' => ['42', 42];
        yield 'Negative digit string' => ['-42', -42];

        yield 'Whole float' => [42.0, 42];
        yield 'Negative whole float' => [-42.0, -42];

        yield 'True' => [true, 1];
        yield 'False' => [false, 0];
    }

    public function data_provider_rejected_integer_inputs(): iterable
    {
        yield 'Empty string' => [''];
        yield 'Space-only string' => [' '];
        yield 'Out of range digit string' => [(string)PHP_INT_MAX . '99'];
        yield 'Out of range negative digit string' => [(string)PHP_INT_MIN . '99'];
        yield 'NAN string' => ['NAN'];
        yield 'Positive infinity string' => ['INF'];
        yield 'Negative infinity string' => ['-INF'];
        yield 'Scientific notation whole number' => ['1e10'];
        yield 'Scientific notation fraction' => ['1e-10'];

        yield 'Non-digit string' => ['forty-two'];
        yield 'String with trailing non-digits' => ['99 red balloons'];
        yield 'String with leading non-digits' => ['Haircut 100'];
        yield 'String with leading space' => [' 42', 42];
        yield 'String with trailing space' => ['42 ', 42];
        yield 'String with leading and trailing space' => [' 42 ', 42];
        yield 'String with trailing zero fraction' => ['42.0', 42];
        yield 'String with trailing non-zero fraction' => ['42.5'];

        yield 'Fractional float' => [42.5];
        yield 'Negative fractional float' => [-42.5];
        yield 'Too large positive float' => [(float)PHP_INT_MAX * 2];
        yield 'Too large negative float' => [(float)PHP_INT_MIN * 2];
        yield 'NaN float' => [NAN];
        yield 'Infinite float' => [INF];

        yield 'Array' => [ [] ];
        yield 'Object' => [new class{}];
        yield 'Resource' => [fopen("data:text/plain,foobar", "r")];
    }

    /**
     * @dataProvider data_provider_valid_integer_inputs
     */
    public function test_type_cast_with_accepted_values($input, int $expectedOutput): void
    {
        $result = type_cast('int', $input);
        $this->assertIsInt($result);
        $this->assertSame($expectedOutput, $result);
    }

    /**
     * @dataProvider data_provider_rejected_integer_inputs
     */
    public function test_type_cast_with_rejected_values($input): void
    {
        $this->expectException(TypeError::class);
        type_cast('int', $input);
    }
}
