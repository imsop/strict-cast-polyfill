<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class FloatTest extends TestCase
{
    public function data_provider_valid_float_inputs(): iterable
    {
        yield 'Integer' => [42, 42.0];
        yield 'Negative Integer' => [-42, -42.0];

        yield 'All-digit string' => ['42', 42.0];
        yield 'Negative digit string' => ['-42', -42.0];
        yield 'String with trailing zero fraction' => ['42.0', 42];
        yield 'String with trailing non-zero fraction' => ['42.5', 42.5];
        yield 'Out of int-range digit string' => [(string)PHP_INT_MAX . '99', (float)PHP_INT_MAX * 100 + 99];
        yield 'Out of int-range negative digit string' => [(string)PHP_INT_MIN . '99', (float)PHP_INT_MIN * 100 - 99];
        yield 'Positive infinity string' => ['INF', INF];
        yield 'Negative infinity string' => ['-INF', -INF];

        yield 'Whole float' => [42.0, 42.0];
        yield 'Negative whole float' => [-42.0, -42.0];
        yield 'Fractional float' => [42.5, 42.5];
        yield 'Negative fractional float' => [-42.5, -42.5];
        yield 'Infinite float' => [INF, INF];

        yield 'True' => [true, 1.0];
        yield 'False' => [false, 0.0];
    }

    public function data_provider_rejected_float_inputs(): iterable
    {
        yield 'Empty string' => [''];
        yield 'Space-only string' => [' '];
        yield 'Non-digit string' => ['forty-two'];
        yield 'String with trailing non-digits' => ['99 red balloons'];
        yield 'String with leading non-digits' => ['Haircut 100'];
        yield 'String with leading space' => [' 42', 42];
        yield 'String with trailing space' => ['42 ', 42];
        yield 'String with leading and trailing space' => [' 42 ', 42];
        yield 'Scientific notation whole number' => ['1e10'];
        yield 'Scientific notation fraction' => ['1e-10'];

        yield 'Array' => [ [] ];
        yield 'Object' => [new class{}];
        yield 'Resource' => [fopen("data:text/plain,foobar", "r")];
    }

    /**
     * @dataProvider data_provider_valid_float_inputs
     */
    public function test_type_cast_with_accepted_values($input, float $expectedOutput): void
    {
        $result = type_cast('float', $input);
        $this->assertIsFloat($result);
        $this->assertSame($expectedOutput, $result);
    }

    /**
     * @dataProvider data_provider_rejected_float_inputs
     */
    public function test_type_cast_with_rejected_values($input): void
    {
        $this->expectException(TypeError::class);
        type_cast('float', $input);
    }

    /**
     * Special copy of test_type_cast_with_accepted_values because NAN !== NAN
     * @return void
     */
    public function test_nan_string(): void
    {
        $input = 'NAN';
        $result = type_cast('float', $input);
        $this->assertIsFloat($result);
        $this->assertNan($result);
    }

    /**
     * Special copy of test_type_cast_with_accepted_values because NAN !== NAN
     * @return void
     */
    public function test_nan_float(): void
    {
        $input = NAN;
        $result = type_cast('float', $input);
        $this->assertIsFloat($result);
        $this->assertNan($result);
    }
}
